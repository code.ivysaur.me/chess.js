# chess.js

![](https://img.shields.io/badge/written%20in-Javascript%20%28node.js%29-blue)

A (bad) chess AI.

Uses some rudimentary depth-first search.


## Download

- [⬇️ chess.js-0.1.7z](dist-archive/chess.js-0.1.7z) *(2.59 KiB)*
